$(document).ready(function(){

    $(".close").on("click", function(){
    
        $(this).parent().hide();
    });

    $(".top-menu__btn.--burger").on("click", function (){
        if($(".top-menu__wrapper").hasClass("--open")){
            $(".top-menu__wrapper").removeClass("--open");
            $(".top-menu__btn.--burger").removeClass("--open");
            $(".top-menu__btn-burger").removeClass("--open");
        }else{
            $(".top-menu__wrapper").addClass("--open");
            $(".top-menu__btn.--burger").addClass("--open");
            $(".top-menu__btn-burger").addClass("--open");
        }
    });
  
  });

